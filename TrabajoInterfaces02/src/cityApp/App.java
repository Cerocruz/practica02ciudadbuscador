package cityApp;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import cityApp.gui.AbstractView;
import cityApp.gui.AcercaDialog;
import cityApp.gui.BuscarView;
import cityApp.gui.CrearView;
import cityApp.gui.LoginView;
import cityApp.modelo.City;
import cityApp.services.CityServiceImpl;
import cityApp.services.NotFoundException;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;

public class App extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8117089293611750323L;

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App frame = new App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private AbstractView view;
	private CityServiceImpl cityServ;

	public App() {
		cityServ = new CityServiceImpl();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 613, 506);
		setResizable(false);
		setLocationRelativeTo(null);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnCiudades = new JMenu("Ciudades");
		menuBar.add(mnCiudades);

		JMenuItem mntmAlta = new JMenuItem("Dar de alta");
		mntmAlta.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK));
		mnCiudades.add(mntmAlta);
		mntmAlta.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				CrearView cView = new CrearView(App.this);
				if (!view.getClass().equals(cView.getClass())) {
					cambiarPantalla(cView);
				}
			}
		});

		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mntmBuscar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_DOWN_MASK));
		mntmBuscar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BuscarView bView = new BuscarView(App.this);
				if (!view.getClass().equals(bView.getClass())) {
					cambiarPantalla(bView);
				}
			}
		});
		mnCiudades.add(mntmBuscar);

		JMenu mnApp = new JMenu("Aplicación");
		menuBar.add(mnApp);

		JMenuItem mntmAcercaDe = new JMenuItem("Acerca de");
		mntmAcercaDe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AcercaDialog dialog = new AcercaDialog(App.this);
				dialog.setVisible(true);
			}
		});

		JMenuItem mntmInicio = new JMenuItem("Inicio");
		mntmInicio.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));
		mnApp.add(mntmInicio);
		mnApp.add(mntmAcercaDe);
		mntmInicio.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				LoginView lView = new LoginView();
				if (!view.getClass().equals(lView.getClass())) {
					cambiarPantalla(lView);
				}
			}
		});

		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "¿Desea salir del programa?", "Confirmar salida",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
					System.exit(0);
				}
			}
		});
		mnApp.add(mntmSalir);

		getContentPane().setLayout(new BorderLayout(0, 0));
		view = new LoginView();
		setContentPane(view);
	}

	public void cambiarPantalla(AbstractView view) {
		setContentPane(view);
		revalidate();
		this.view = view;
		this.view.inicializar();
	}

	public List<City> buscarPorFiltro(String filtro) {
		try {
			try {
				cityServ.getCity(Long.parseLong(filtro));
				List<City> ciudad = new ArrayList<>();
				ciudad.add(cityServ.getCity(Long.parseLong(filtro)));
				return ciudad;
			} catch (NumberFormatException excepcion) {
				return cityServ.getCities(filtro);
			}
		} catch (NotFoundException e) {
			JOptionPane.showMessageDialog(view, "Error: " + e.getMessage(), "Not Found", 1);
			return null;
		}
	}

	public City crearCiudad(City city) {
		return cityServ.createCity(city);
	}
}
