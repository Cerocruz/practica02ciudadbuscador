package cityApp.gui;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import cityApp.App;
import cityApp.modelo.City;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.FlowLayout;

public class BuscarView extends AbstractView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5693561748396339554L;

	private JTable table;
	private JPanel panel;
	private JButton btnConsultar;
	private JTextField fldBuscador;
	private JPanel panel_1;
	private TableModel tModel;

	public BuscarView(App app) {
		super();
		setLayout(new BorderLayout(0, 0));
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		tModel = new TableModel();
		table.setModel(tModel);

		panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		fldBuscador = new JTextField();
		panel_1.add(fldBuscador);
		fldBuscador.setColumns(10);
		fldBuscador.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					btnConsultar.doClick();
				}

			}

		});

		btnConsultar = new JButton("Buscar");
		panel_1.add(btnConsultar);
		btnConsultar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					List<City> list = app.buscarPorFiltro(fldBuscador.getText());
					if (list == null) {
						list = new ArrayList<>();
					}
					tModel.setList(list);
				} finally {
					tModel.fireTableDataChanged();
					fldBuscador.setText("");
					fldBuscador.requestFocus();
				}
			}
		});

	}

	@Override
	public void inicializar() {
		fldBuscador.requestFocus();
	}

}
