package cityApp.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import cityApp.modelo.City;

public class TableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1643014677199515030L;

	private List<City> list;
	private List<String> columnName;

	public TableModel() {
		list = new ArrayList<>();
		columnName = new ArrayList<>();
		columnName.add("ID");
		columnName.add("DESCRIPCION");
		columnName.add("COUNTRY ID");
	}

	public List<City> getList() {
		return list;
	}

	public void setList(List<City> list) {
		this.list = list;
	}

	public List<String> getColumnName() {
		return columnName;
	}

	public void setColumnName(List<String> columnName) {
		this.columnName = columnName;
	}

	@Override
	public String getColumnName(int column) {
		return columnName.get(column);
	}

	@Override
	public int getRowCount() {
		return list.size();
	}

	@Override
	public int getColumnCount() {
		return columnName.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		String columnStr = columnName.get(columnIndex);
		if (columnStr.equals("ID")) {
			return list.get(rowIndex).getId();
		} else if (columnStr.equals("DESCRIPCION")) {
			return list.get(rowIndex).getDescripcion();
		} else {
			return list.get(rowIndex).getCountryId();
		}
	}

}
