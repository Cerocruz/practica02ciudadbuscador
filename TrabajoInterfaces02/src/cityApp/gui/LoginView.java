package cityApp.gui;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;

public class LoginView extends AbstractView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2815752167594733886L;

	public LoginView() {
		setBackground(Color.WHITE);
		JLabel lblPicture;
		setLayout(null);
		lblPicture = new JLabel(new ImageIcon("src/imagenes/logo.png"));
		lblPicture.setBounds(113, 0, 362, 265);
		add(lblPicture);

		add(lblPicture);
		JLabel lblWelcome = new JLabel("BIENVENIDO A CIUDAD BUSCADOR");
		lblWelcome.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 26));
		lblWelcome.setBounds(123, 276, 357, 86);
		add(lblWelcome);

		JLabel lblNewLabel = new JLabel("Hecho por: César Romero de la Cruz");
		lblNewLabel.setForeground(new Color(192, 192, 192));
		lblNewLabel.setBounds(269, 359, 206, 25);
		add(lblNewLabel);
	}

	@Override
	public void inicializar() {

	}
}
