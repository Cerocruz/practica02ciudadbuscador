package cityApp.gui;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import cityApp.App;
import cityApp.modelo.City;

import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;


public class CrearView extends AbstractView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3758913213762487805L;

	private JTable table;
	private TableModel tModel;
	private JTextField fldDescripcion;
	private JTextField fldIdPais;
	private JButton btnCrear;
	private JLabel lblUltimaCiudad;

	public CrearView(App app) {
		setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 105, 613, 401);
		add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		tModel = new TableModel();
		table.setModel(tModel);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 613, 105);
		add(panel);
		panel.setLayout(null);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 0, 613, 105);
		panel.add(panel_1);
		panel_1.setLayout(null);

		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setBounds(30, -3, 113, 39);
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(lblDescripcion);

		fldDescripcion = new JTextField();
		fldDescripcion.setBounds(153, 11, 139, 17);
		panel_1.add(fldDescripcion);
		fldDescripcion.setColumns(15);
		fldDescripcion.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					fldIdPais.requestFocus();
				}
			}

		});

		JLabel lblIdPais = new JLabel("Id Pais");
		lblIdPais.setBounds(331, 0, 103, 39);
		lblIdPais.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(lblIdPais);

		fldIdPais = new JTextField();
		fldIdPais.setBounds(418, 11, 153, 17);
		fldIdPais.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				int key = e.getKeyChar();
				if (!(key >= 48 && key <= 57)) {
					e.consume();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					btnCrear.doClick();
				}
			}

		});

		panel_1.add(fldIdPais);
		fldIdPais.setColumns(5);

		btnCrear = new JButton("Crear");
		btnCrear.setBounds(185, 44, 249, 28);
		panel_1.add(btnCrear);

		lblUltimaCiudad = new JLabel("Ultima ciudad -->");
		lblUltimaCiudad.setBounds(0, 91, 613, 14);

		panel_1.add(lblUltimaCiudad);

		btnCrear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String descripcion = fldDescripcion.getText();
				String idPais = fldIdPais.getText();
				List<City> list = tModel.getList();
				if (!descripcion.equals("") && !idPais.equals("")) {
					City city = app.crearCiudad(new City(descripcion, Long.parseLong(idPais)));
					list.add(city);
					tModel.fireTableDataChanged();
					City ciudad = list.get(list.size() - 1);

					lblUltimaCiudad.setText("Última ciudad --> Id: " + ciudad.getId() + "|| Descripcion: "
							+ ciudad.getDescripcion() + "|| Id Pais: " + ciudad.getCountryId());
				} else {
					JOptionPane.showMessageDialog(CrearView.this, "Error: Parametros 'Descripcion' o 'Id Pais' vacios",
							"Parametros vacios", 1);
					fldDescripcion.requestFocus();
					return;
				}
				fldDescripcion.setText("");
				fldIdPais.setText("");
				fldDescripcion.requestFocus();
			}

		});
	}

	@Override
	public void inicializar() {
		fldDescripcion.requestFocus();
	}
}
