package cityApp.gui;

import javax.swing.JPanel;

public abstract class AbstractView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4923883168180874698L;

	public AbstractView() {
		setBounds(100, 100, 613, 506);
	}

	public abstract void inicializar();

}
