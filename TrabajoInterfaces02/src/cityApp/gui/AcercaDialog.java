package cityApp.gui;

import javax.swing.JDialog;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import cityApp.App;
import javax.swing.JTextPane;

public class AcercaDialog extends JDialog {

	public AcercaDialog(App app) {
		super(app, true);
		setBounds(app.getX() + 50, app.getY() + 100, 500, 300);
		setResizable(false);
		getContentPane().setLayout(null);
		getContentPane().setBackground(Color.WHITE);

		JButton btnCerrar = new JButton("Cerrar");
		btnCerrar.setBounds(172, 227, 89, 23);
		btnCerrar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AcercaDialog.this.setVisible(false);
			}
		});
		getContentPane().add(btnCerrar);

		JLabel lblLogo = new JLabel(new ImageIcon("src/imagenes/logo.png"));
		lblLogo.setBounds(214, 0, 270, 228);
		getContentPane().add(lblLogo);

		JTextPane txtpnInformacion = new JTextPane();
		txtpnInformacion.setBackground(null);
		txtpnInformacion.setText(
				"Esta aplicación creada por Blas de los Blas en 1939 está diseñada para que la experiencia de buscar "
						+ "tus ciudades sea lo más placentera posible dando la posibilidad de introducir las tuyas propias.");
		txtpnInformacion.setBounds(20, 32, 200, 184);
		txtpnInformacion.setFocusable(false);

		getContentPane().add(txtpnInformacion);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6437556619964027578L;
}
